//ticketPrint
var ticketPrint = {
    /**
	* 初始商品编码
	*/
    initCoods: [],
    /**
	* 获取商品信息
	*/
    getGoodsInfo: function (coods) {
        var self = this;
        //注：给出的示例数据，有同一个编码出现几次，也有同一个编码出现一次但是带了数量，所以这里考虑一个场景，收银员可能会把每个商品扫描一次，也可能会同样的产品扫描多次，为了防止多次发送请求，先对这个数据做一次去重，并把这批编码发送给后端获取对应的数据信息


        self.initCoods = coods; //讲初始编码数组保存为属性

        //1.先整理一次去掉数量的数组
        //注：对数组和json对象的处理，这里都使用了一个第三方库：Underscore.js
        var data = [];
        _.each(coods, function (item) {
            data.push(item.split("-")[0]);
        });

        //2.再对去掉商品数量的数组进行去重处理
        data = _.uniq(data);

        //3.TODO 将去重后的编码数组data发送给服务端，服务端返回对应编码的商品信息
        //注：个人只是前端，所以这里只写一个静态的json作为示例
        var goodInfos = {
            "ITEM000001": {
                code: "ITEM000001",
                name: "可口可乐",
                units: "瓶",
                Price: "3.00",
                category: "食品",
                isFullCut: false,
                isDiscount: false
            },
            "ITEM000003": {
                code: "ITEM000003",
                name: "羽毛球",
                units: "个",
                Price: "1.00",
                category: "食品",
                isFullCut: true,
                isDiscount: true
            },
            "ITEM000005": {
                code: "ITEM000005",
                name: "苹果",
                units: "斤",
                Price: "5.50",
                category: "水果",
                isFullCut: true,
                isDiscount: false
            },
            "ITEM000006": {
                code: "ITEM000006",
                name: "香蕉",
                units: "斤",
                Price: "2.50",
                category: "水果",
                isFullCut: false,
                isDiscount: true
            }
        }

        //4.获取到商品基本信息后，需要计算每个商品总共购买的数量，这个部分返回带商品数量的商品信息
        var coods = [];
        var num = 1;
        var good;
        _.each(self.initCoods, function (item) {//对扫描的编码做一个遍历，计算出每个商品的数量
            coods = item.split("-");
            num = parseInt(coods[1]) || 1;//注意输入的编码里面可能会带有数量
            good = goodInfos[coods[0]];
            if (good) {
                good.num = good.num + num || num;
            }
        });

        //TODO下面这一步可以是为了防止没有输入某个商品时做的容错处理，正常是不需要的。
        _.each(goodInfos, function (item) {
            if (!item.num) {
                delete goodInfos[item.code];
            }
        })

        self.setGoodsInfo(goodInfos);
    },
    /**
	* 根据商品信息json生成输出的html信息
	*/
    setGoodsInfo: function (goodInfos) {

        var html = "<p>***<没钱赚商店>购物清单***</p>";
        var fullCutHtml = "";
        var isSave = false;
        var totalHtml = "";

        var totalPrice = 0;
        var savePrice = 0;

        //遍历商品记录生成小票html
        _.each(goodInfos, function (item) {

            var price = item.Price; //单价
            var num = item.num;//数量
            var finalNum = num;//实际计算单价的数量
            var subtotal = 0;//小计

            var saveHtml = "";//折扣节省html

            if (item.isFullCut) {//满2免1
                var cutNum = (num - num % 3) / 3;
                finalNum = cutNum * 2 + num % 3;
                subtotal = finalNum * price;

                //输出满2赠1HTML
                if (cutNum) {//满2赠1，用户只买了2个，则输出满赠HTML
                    fullCutHtml += "<p>名称：" + item.name + "，数量：" + cutNum + item.units + "</p>";
                }

            } else if (item.isDiscount) {//95折扣

                if (!isSave) isSave = true;

                discount = 0.95;
                subtotal = num * price * 0.95;
                saveHtml += "，节省" + num * price * 0.05.toFixed(2) + "(元)";
                savePrice += num * price * 0.05;//计算节省的总计

            } else {
                subtotal = num * price;
            }

            totalPrice += subtotal;//计算总计

            saveHtml += "</p>";
            html += "<p>名称：" + item.name + "，数量：" + num + item.units + "，单价：" + price + "(元)，小计：" + subtotal.toFixed(2) + "(元)" + saveHtml;

        });
        html += "----------------------";

        //生成满2减1html
        if (fullCutHtml) {
            "<p>买二赠一商品：</p>"
            fullCutHtml += "----------------------";
            html += fullCutHtml;
        }

        //生成总计html
        totalHtml += "<p>总计：" + totalPrice.toFixed(2) + "(元)</p>"
        if (isSave) {
            totalHtml += "<p>节省：" + savePrice.toFixed(2) + "(元)</p>"
        }
        totalHtml += "<p>**********************</p>";
        html += totalHtml;

        $("#js_content").html(html);
        var double = $("<p>买二赠一商品：</p>");
        var total = $("<p>总计：20.45(元)</p>");

    },
    /**
	* 获取打印信息
	*/
    getInfo: function (coods) {
        this.getGoodsInfo(coods);
    }
}